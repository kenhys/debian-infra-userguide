## UDD bapase.cgi解説

### はじめに

いくつかの基準でパッケージを表示するためのCGIです。
 
https://udd.debian.org/bapase.cgi にアクセスすると、次のリストが表示されます。

* Orphaned packages
* RFAed packages
* Packages maintained with NMUs
* Packages not in testing
* Packages not maintained by DDs
* Packages maintained or co-maintained by people with lots of NMUs

「Orphaned packages」はメンテナンスが放棄されたパッケージです。
「RFAed packages」は新しいメンテナを募集しているパッケージです。
「Packages maintained with NMUs」は決まったメンテナのいないパッケージです。
「Packages not in testing」はtestingに存在しないパッケージです。なんらかのバグによりパッケージが削除されたなどの状態です。
「Packages not maintained by DDs」はDebian開発者によってメンテナンスされていないパッケージです。
「Packages maintained or co-maintained by people with lots of NMUs」は決まったメンテナが長いこといないパッケージです。

### bapase.cgiのパラメーター

* t

タイプを指定する。 
指定できるタイプは次のとおり。 

* o
  * orphanedなパッケージを表示します。ソート順はorphanedされてからの日数の降順で表示します。
* o2
  * orphanedなパッケージを表示します。ソート順はorphanedされてからの日数の降順で表示します。oとの違いはorphanedされてからの日数が600日以上であるものに限定されていることです。
* rfa
  * RFAなパッケージを表示します。ソート順はorphanedされてからの日数の降順で表示します。oとの違いはorphanedされてからの日数が600日以上であるものに限定されていることです。
* nmu
  * orphanedなパッケージで、複数回NMUされたパッケージを表示します。
* testing
  * orphanedなパッケージで、testingにパッケージがないものを表示します。
* nodd
  * orphanedなパッケージでDebian開発者によってメンテナンスされていないパッケージを表示します。
* maintnmu
  * orphanedなパッケージで複数の開発者によってNMUとしてメンテナンスされているパッケージを表示します。
