## bugreport.cgi解説

### はじめに

#### bugreport.cgiのパラメーター

bugreport.cgiが受け付けるパラメーターは次のとおりです。

* bug
* msg
* att
* boring
* terse
* reverse
* mbox
* mime
* trim
* mboxstat
* mboxmaint
* archive
* repeatmerged
* avatars

### bug

バグ番号を指定します。
バグ番号が指定されていないと、400エラーとなります。
バグ番号に数値でないものを指定しても400エラーとなります。

### msg

メッセージ番号を指定します。
メッセージ番号を指定すると、該当するメッセージだけを表示します。

バグ報告の表示範囲をしぼることなく、該当メッセージの先頭を表示した場合には、`#数値`を指定します。

### att

添付ファイル番号を指定します。
添付ファイル番号を指定すると、

FIXME:

### boring

バグ報告のうち、重要度の低いステータスの変更などのメッセージの表示の可否を指定します。

既定値はnoで、yesにすると、FIXME:

### terse

FIXME:

### reverse

メッセージの表示順を指定します。
既定値はnoで昇順で表示し、yesにすると降順で表示します。

### mbox

既定値はnoです。

### mime

既定値はnoです。

### trim

### mboxstat

既定値はnoです。

### mboxmaint

既定値はnoです。

### archive

既定値はnoです。

### repeatmerged

既定値はnoです。

### avatars

バグ報告を表示する際、アバターの表示を有効にします。
アバターの表示があることで誰がコメントしたのかが区別しやすくなります。

既定値はyesで、有効になっています。

