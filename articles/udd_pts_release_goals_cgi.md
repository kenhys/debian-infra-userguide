## UDD pts-release-goals.cgi解説

### はじめに

https://udd.debian.org/pts-release-goals.cgi はtext/yamlで
https://release.debian.org/testing/goals.yaml を読み込んで
リリースのためにpending状態にあるバグを表示します。

しかし、2023年2月現在、 https://release.debian.org/testing/goals.yaml にアクセスできないので、このCGIは機能しません。
