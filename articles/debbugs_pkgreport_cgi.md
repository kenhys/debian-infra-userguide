## pkgreport.cgi解説

### はじめに

HEAD/GETで

### pkgreport.cgiのパラメーター

* ordering
* archive
* repeatmerged
* include
* exclude
* bug-rev
* pend-rev
* sev-rev
* maxdays
* mindays
* version
* data
* which
* dist
* noaffects

### ordering

`ordering`には次の値を指定できます。

* `normal`
* `oldview`
* `raw`
* `age`

既定値は`normal`です。

### archive

既定値は0です。

### repeatmerged

既定値は0です。

### include

既定値はありません。

### exclude

既定値はありません。

### bug-rev

既定値はありません。

### pend-rev

### sev-rev
### maxdays
### mindays
### version
### data
### which
### dist
### noaffects
