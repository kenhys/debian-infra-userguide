## version.cgi解説

### はじめに

バグレポートに含まれる、報告されたバージョンと影響のあるバージョンをグラフとして可視化するためのCGIです。

https://bugs.debian.org/cgi-bin/version.cgi 

https://bugs.debian.org/cgi-bin/version.cgi?collapse=1;package=fcitx-mozc-data;height=2;found=mozc%2F2.26.4220.100%2Bdfsg-5.1;width=2;absolute=0;fixed=mozc%2F2.26.4220.100%2Bdfsg-5.2

### version.cgiのパラメーター

* query
* single
* package
* found
* fixed
* ignore_boring
* collapse
* format
* width
* height
* info

### query

### single

`single` には次の値を指定できます。

* package パッケージ名を指定します
* format 出力するフォーマットを指定します。既定はSVGです。png/svg/jpg/gifを
* ignore_boring 既定は1です。
* width 画像の幅を指定します。
* height 画像の高さを指定します。
* collapse 状況に変化のないバージョンを省略します。既定は0で省略しません。
* info ETAGをみて、なければ304を返すかどうかを指定します。既定は0です。

例えば、fcitx-mozc-dataパッケージのバグ修正の例をみてみましょう。

https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1009254

```
https://bugs.debian.org/cgi-bin/version.cgi?collapse=1;\
  package=fcitx-mozc-data;height=2;\
  found=mozc%2F2.26.4220.100%2Bdfsg-5.1;\
  width=2;absolute=0;fixed=mozc%2F2.26.4220.100%2Bdfsg-5.2
```

