## UDD sponsorstats.cgi解説

### はじめに

https://udd.debian.org/sponsorstats.cgi は
Debian開発者がどれくらいパッケージのスポンサーをしているかを
表示するCGIです。

Debian開発者がどの人のスポンサーをしたのか、
またスポンサーした人ごとのアップロード回数も表示してくれます。
