# バグトラッキングシステムを歩く

## はじめに

Debianのバグトラッキングシステムは https://www.debian.org/Bugs/ として
アクセスできるようになっています。

バグトラッキングシステムのソースコードはDebianプロジェクトで運用している GitLabのインスタンスにて公開されており、debbugs と呼ばれています。

https://salsa.debian.org/debbugs-team/debbugs

https://wiki.debian.org/Teams/Debbugs

GitHubやGitLabなどのチケット管理とは異なり、バグ報告の手段はメールである
というのはやや古めかしいと感じる人もいることでしょう。

## バグ報告の支援ツール

reportbugについて解説する


* bugreport.cgi
* libravatar.cgi
* pkgindex.cgi
* pkgrepot.cgi
* search.cgi
* version.cgi

